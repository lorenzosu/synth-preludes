Synth Prelude n. 3
==================

Author: Lorenzo Sutton
----------------------

License: [CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/)
--------------------------------------------------------------------------

A Synth Prelude made with Yoshimi and Rosegarden


Source [Rosegarden](https://www.rosegardenmusic.com/) and
[Yoshimi](http://yoshimi.sourceforge.net/) files.


To play the files:
- Start [Jack](http://jackaudio.org/)
- Start Rosegarden and load the synth_prelude_03.rg file
- Start Yoshimi making sure that 'alsa' is selected as MIDI input
(Settings... > Alsa > 'Set as preferred midi'
- Load the synth_prelude_03.state state file
- Connect the rosegarden midi output called 'Yoshimi' to yoshimi (via alsa midi)


Link to audio:
https://archive.org/details/synth_prelude_03
